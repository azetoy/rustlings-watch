// primitive_types6.rs
// Use a tuple index to access the second element of `numbers`.
// You can put this right into the `println!` where the ??? is.
// Execute `rustlings hint primitive_types6` for hints!

// I AM DONE

fn main() {
    let (numbers1,numbers2,last) = (1, 2, 3);
    println!("The first number is {}",numbers1);
    println!("The second number is {}", numbers2);
    println!("The last number is {}",last);
}
